package com.company;

public class Main {

    public static void main(String[] args) {
	Users Kolia = new Users();
	Kolia.name = "Коля";
	Kolia.age = 11;

	Users Olia = new Users();
	Olia.name = "Оля";
	Olia.age = 18;

	Kolia.hasAccess();
	Olia.hasAccess();
    }

}
 class Users {
    int age;
    String name;

    public void hasAccess(){
        if (age <18 ){
            System.out.println("Hello" + name +", access is denied!");
        } else
            System.out.println("Welcome " + name);
    }
}